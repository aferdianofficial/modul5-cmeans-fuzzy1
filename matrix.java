public class matrix {
	public double k1;
	public double k2;
	public double kw1;
	public double kw2;	
	public matrix() {
    	this.k1 = k1;
		this.k2 = k2;
		this.kw1 = kw1;
		this.kw2 = kw2;
    }
	public void setData1(double x){
		this.k1 = x;
	}
	public void setData2(double x){
		this.k2 = x;
	}
	public void setDataValues(double x1, double x2){
		this.k1 = x1;
		this.k2 = x2;
		this.kw1 = Math.pow(k1, 2);
		this.kw2 = Math.pow(k2, 2);
	}
	public double getData_k1(){
		return k1;
	}
	public double getData_k2(){
		return k2;
	}
	public double getData_kw1(){
		return kw1;
	}
	public double getData_kw2(){
		return kw2;
	}
}
