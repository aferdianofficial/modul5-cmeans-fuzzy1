public class tabel_jarak {
	public double c1;
	public double c2;
	public double bobot;
	public double k;
	public double bkw;
    public tabel_jarak() {
    	this.c1 = c1;
    	this.c2 = c2;
    	this.bobot = bobot;
    	this.k = k;
    	this.bkw = bkw;
    }
    public void setDataC1(double x){
    	this.c1 = x;
    }
    public void setDataC2(double x){
    	this.c2 = x;
    }
    public void setDataBobot(double x1, double x2){
    	this.bobot = x1 + x2;
    }
    public void setDataK(double x){
    	this.k = x;
    }
    public void setDatabkw(double b, double k){
    	this.bkw = b * Math.pow(k, 2);
    }
    public double getDataC1(){
    	return c1;
    }
    public double getDataC2(){
    	return c2;
    }
    public double getDataBobot(){
    	return bobot;
    }
    public double getDataK(){
    	return k;
    }
    public double getDatabkw(){
    	return bkw;
    }
}