import java.util.Random;
import java.text.*;
public class fuzzy_cmeans {
	DecimalFormat numberFormat = new DecimalFormat("#.#");
	DecimalFormat numberFormat2 = new DecimalFormat("#.##");
	dataset array[] = new dataset[6];
	dataset partition[] = new dataset[6];
	matrix m[] = new matrix[6];
	dataset kwa1[] = new dataset[6];
	dataset kwa2[] = new dataset[6];
	dataset cluster[] = new dataset[2];
	tabel_jarak t1[] = new tabel_jarak[6];
	tabel_jarak t2[] = new tabel_jarak[6];
	double sum_kw1 = 0, sum_kw2 = 0;
	double sum_kwa1_1 = 0, sum_kwa1_2 = 0;
	double sum_kwa2_1 = 0, sum_kwa2_2 = 0;
	double sum_bkw1 = 0, sum_bkw2 = 0; 
	public fuzzy_cmeans(){
		setDataset();
		setPartition();
		setMatrix();
		setKwA1();
		setKwA2();
		setCluster();
		setTabelJarak();
		System.out.println("Dataset :");
		printData();
		System.out.println("Isi tabel matriks:");
		printMatrix();
		System.out.println("Isi dari tabel matriks x dataset:");
		printKwA();
		System.out.println("Cluster : ");
		printCluster();
		printTabelJarak();
	}		
	void setDataset(){
		array[0] = new dataset();
		array[0].setDataValues(35, 3);
		array[1] = new dataset();
		array[1].setDataValues(22, 2);
		array[2] = new dataset();
		array[2].setDataValues(60, 1);
		array[3] = new dataset();
		array[3].setDataValues(59, 1);
		array[4] = new dataset();
		array[4].setDataValues(25, 1);
		array[5] = new dataset();
		array[5].setDataValues(37, 4);
    }
    void printData(){
    	for(int i = 0; i < 6; i++){
    		System.out.println(array[i].getData1() + " " + array[i].getData2());
    	}
    }    
    void setPartition(){
    	for(int i = 0; i < 6 ; i++){
    		partition[i] = new dataset();
    		partition[i].setData1(Math.random() * 1 + 0);
    		partition[i].setData2(1 - partition[i].getData1());
    	}
    }
    void printPartition(){
    	for(int i = 0; i < 6; i++){
    		System.out.println(numberFormat.format(partition[i].getData1()) + " " + numberFormat.format(partition[i].getData2()));
    	}
    }
    void setMatrix(){
    	for(int i = 0; i < 6; i++){
    		m[i] = new matrix();
    		m[i].setDataValues(partition[i].getData1(), partition[i].getData2());
    		sum_kw1 += m[i].getData_kw1();
    		sum_kw2 += m[i].getData_kw2();
    	}
    }
    void printMatrix(){
    	for(int i = 0; i < 6; i++){
    		System.out.println(numberFormat.format(m[i].getData_k1()) + " " + numberFormat.format(m[i].getData_k2()) + " " + numberFormat2.format(m[i].getData_kw1()) + " " + numberFormat2.format(m[i].getData_kw2()));
    	}
    	System.out.println(numberFormat2.format(sum_kw1));
    	System.out.println(numberFormat2.format(sum_kw2));
    }
	    void setKwA1(){
    	for(int i = 0; i < 6; i++){
    		kwa1[i] = new dataset();
    		kwa1[i].setData1(m[i].getData_kw1() * array[i].getData1());
    		kwa1[i].setData2(m[i].getData_kw2() * array[i].getData1());
    		sum_kwa1_1 += kwa1[i].getData1();
    		sum_kwa1_2 += kwa1[i].getData2();
    	}
    }
    void setKwA2(){
    	for(int i = 0; i < 6; i++){
    		kwa2[i] = new dataset();
    		kwa2[i].setData1(m[i].getData_kw1() * array[i].getData2());
    		kwa2[i].setData2(m[i].getData_kw2() * array[i].getData2());
    		sum_kwa2_1 += kwa2[i].getData1();
    		sum_kwa2_2 += kwa2[i].getData2();
    	}
    }
    void printKwA(){
    	for(int i = 0; i < 6; i++){
    		System.out.println(numberFormat2.format(kwa1[i].getData1()) + " " + numberFormat2.format(kwa1[i].getData2()));
    	}
    	for(int i = 0; i < 6; i++){
    		System.out.println(numberFormat2.format(kwa2[i].getData1()) + " " + numberFormat2.format(kwa2[i].getData2()));
    	}
    	System.out.println(numberFormat2.format(sum_kwa1_1));
    	System.out.println(numberFormat2.format(sum_kwa1_2));
    	System.out.println(numberFormat2.format(sum_kwa2_1));
    	System.out.println(numberFormat2.format(sum_kwa2_2));
	}
	    void setCluster(){
    		cluster[0] = new dataset();
    		cluster[0].setData1(sum_kwa1_1 / sum_kw1);
    		cluster[0].setData2(sum_kwa1_2 / sum_kw1);
    		cluster[1] = new dataset();
    		cluster[1].setData1(sum_kwa2_1 / sum_kw2);
    		cluster[1].setData2(sum_kwa2_2 / sum_kw2);
    }
    void printCluster(){
    	System.out.println(cluster[0].getData1() + " " + cluster[0].getData2());
    	System.out.println(cluster[1].getData1() + " " + cluster[1].getData2());
    }
    void setTabelJarak(){
    	for(int i = 0; i < 6; i++){
    		t1[i] = new tabel_jarak();
    		t1[i].setDataC1(Math.pow((array[i].getData1() - cluster[0].getData1()) ,2));
    		t1[i].setDataC2(Math.pow((array[i].getData2() - cluster[0].getData2()) ,2));
    		t1[i].setDataBobot(t1[i].getDataC1(), t1[i].getDataC2());
    		
    		t2[i] = new tabel_jarak();
    		t2[i].setDataC1(Math.pow((array[i].getData1() - cluster[1].getData1()) ,2));
    		t2[i].setDataC2(Math.pow((array[i].getData2() - cluster[1].getData2()) ,2));
    		t2[i].setDataBobot(t2[i].getDataC1(), t2[i].getDataC2());
    		
    		t1[i].setDataK(t1[i].getDataBobot() / (t1[i].getDataBobot() + t2[i].getDataBobot()));
    		t2[i].setDataK(t2[i].getDataBobot() / (t1[i].getDataBobot() + t2[i].getDataBobot()));
    		
    		t1[i].setDatabkw(t1[i].getDataBobot(), t1[i].getDataK());
    		t2[i].setDatabkw(t2[i].getDataBobot(), t2[i].getDataK());
    		
    		sum_bkw1 += t1[i].getDatabkw();
    		sum_bkw2 += t2[i].getDatabkw();
    	}
    }
    void printTabelJarak(){
    	System.out.println("Tabel Jarak Cluster 1");
    	for(int i = 0; i < 6; i++){
    		System.out.println(t1[i].getDataC1() + " " + t1[i].getDataC2() + " " + t1[i].getDataBobot() + " " + t1[i].getDataK() + " " + t1[i].getDatabkw());
    	}
    	System.out.println("Tabel Jarak Cluster 2");
    	for(int i = 0; i < 6; i++){
    		System.out.println(t2[i].getDataC1() + " " + t2[i].getDataC2() + " " + t2[i].getDataBobot() + " " + t2[i].getDataK() + " " + t2[i].getDatabkw());
    	}
    	System.out.print("Total nilai bkw1: ");
    	System.out.println(sum_bkw1);
    	System.out.print("Total nilai bkw2: ");
    	System.out.println(sum_bkw2);
    	System.out.print("Nilai p0: ");
    	System.out.println(sum_bkw1 + sum_bkw2);
    }
    
    public static void main(String[] args) {
        fuzzy_cmeans fc = new fuzzy_cmeans();
    }